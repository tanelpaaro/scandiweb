<?php

namespace App;

/**
 * Application configuration
 */
class Config
{
    /**
     * Database host
     *
     * @var string
     */
    const DB_HOST = 'db';

    /**
     * Database name
     *
     * @var string
     */
    const DB_NAME = 'scandiweb';

    /**
     * Database user
     *
     * @var string
     */
    const DB_USER = 'user';

    /**
     * Database password
     *
     * @var string
     */
    const DB_PASSWORD = 'password';

    /**
     * Show or hide error messages on screen
     *
     * @var bool
     */
    const SHOW_ERRORS = false;
}
