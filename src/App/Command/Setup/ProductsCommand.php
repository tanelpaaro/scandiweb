<?php

namespace App\Command\Setup;

use App\Model\Attribute;
use App\Model\Product;
use App\Model\ProductAttributeValue;
use App\Service\ProductService;
use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
use Exception;
use OutOfBoundsException;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class ProductsCommand extends Command
{
    /**
     * Number of products created by default
     *
     * @var int
     */
    private const PRODUCT_COUNT = 12;

    protected static $defaultName = 'app:setup:products';

    private $specialAttributes;

    protected function configure(): void
    {
        $this->addArgument(
            'count',
            InputArgument::OPTIONAL,
            'How many products do you want to create?',
            self::PRODUCT_COUNT
        );
    }

    /**
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $productTypes = ProductService::getTypes();
        $count = (int) $input->getArgument('count');
        for ($i = 0; $i < $count; $i++) {
            $type = array_rand($productTypes, 1);

            $product = new Product();
            $product->setSku($this->generateSku($type));
            $product->setName($this->generateName($type));
            $product->setPrice(round(rand(1, 100) . '.' . rand(0, 99), 1));
            $product->setType($type);

            $specialAttributes = $this->getSpecialAttributes($type);
            foreach ($specialAttributes as $attribute) {
                $productAttributeValue = new ProductAttributeValue();
                $productAttributeValue->setAttribute($attribute);
                $productAttributeValue->setProduct($product);
                $productAttributeValue->setValue($this->getRandomValue($attribute));
                $product->addSpecialAttribute($productAttributeValue);
            }

            $saved = false;
            while (!$saved) {
                try {
                    $product->save();
                    $saved = true;
                } catch (UniqueConstraintViolationException $e) {
                    $product->setSku($this->generateSku($type));
                } catch (Exception $e) {
                    // fail-safe
                    $saved = true;
                }
            }
        }

        return Command::SUCCESS;
    }

    private function generateSku(string $productType): string
    {
        return strtoupper(uniqid($productType . '-'));
    }

    private function generateName(string $productType): string
    {
        switch ($productType) {
            case Product::TYPE_DVD:
                $values = ['Acme', 'Sony', 'MEDIARANGE'];

                break;
            case Product::TYPE_BOOK:
                $values = ['In Search of Lost Time', 'Ulysses', 'Don Quixote'];

                break;
            case Product::TYPE_FURNITURE:
                $values = ['Chair', 'Sofa', 'Desk'];

                break;
            default:
                throw new OutOfBoundsException('Unmapped product type: ' . $productType);

                break;
        }

        shuffle($values);

        return current($values);
    }

    /**
     * @return string|array
     */
    private function getRandomValue(Attribute $attribute)
    {
        $valueSuffix = null;
        switch ($attribute->getAttributeCode()) {
            case Attribute::ATTRIBUTE_SIZE:
                $values = [65, 210, 700];
                $valueSuffix = ' MB';

                break;
            case Attribute::ATTRIBUTE_WEIGHT:
                $values = [0.1, 0.2, 0.3];
                $valueSuffix = ' KG';

                break;
            case Attribute::ATTRIBUTE_DIMENSIONS:
                $values = [[100, 200, 300], [400, 500, 600], [1000, 3000, 2000]];

                break;
            default:
                throw new OutOfBoundsException('Unmapped attribute: ' . $attribute->getAttributeCode());

                break;
        }

        shuffle($values);

        $value = current($values);
        if (!is_array($value) && $valueSuffix) {
            $value .= $valueSuffix;
        }

        return  $value;
    }

    private function getSpecialAttributes(string $productType): array
    {
        if (null === $this->specialAttributes) {
            $attributes = Attribute::getList();
            foreach ($attributes as $attribute) {
                $allowedTypes = $attribute->getAllowedTypes();
                foreach ($allowedTypes as $type) {
                    $this->specialAttributes[$type][] = $attribute;
                }
            }
        }

        return $this->specialAttributes[$productType] ?? [];
    }
}
