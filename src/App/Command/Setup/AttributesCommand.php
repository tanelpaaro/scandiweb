<?php

namespace App\Command\Setup;

use App\Model\Attribute;
use App\Model\Product;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class AttributesCommand extends Command
{
    protected static $defaultName = 'app:setup:attributes';

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $attribute = new Attribute();
        $attribute->setAttributeCode('size');
        $attribute->setAttributeName('Size');
        $attribute->setAllowedTypes([Product::TYPE_DVD]);
        $attribute->setNote('In megabytes');
        $attribute->save();

        $attribute = new Attribute();
        $attribute->setAttributeCode('weight');
        $attribute->setAttributeName('Weight');
        $attribute->setAllowedTypes([Product::TYPE_BOOK]);
        $attribute->setNote('In kilograms');
        $attribute->save();

        $attribute = new Attribute();
        $attribute->setAttributeCode('dimensions');
        $attribute->setAttributeName('Dimensions');
        $attribute->setAllowedTypes([Product::TYPE_FURNITURE]);
        $attribute->setNote('In millimetres');
        $attribute->save();

        return Command::SUCCESS;
    }
}
