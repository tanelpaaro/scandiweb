<?php

namespace App\Twig;

use App\Model\Attribute;
use App\Model\Product;
use App\Service\ProductService;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

class AppExtension extends AbstractExtension
{
    public function getFunctions()
    {
        return [
            new TwigFunction('app_attribute_list', [$this, 'getAttributeList']),
            new TwigFunction('app_attribute_template', [$this, 'getAttributeTemplate']),
            new TwigFunction('app_product_list', [$this, 'getProductList']),
            new TwigFunction('app_product_typeList', [$this, 'getProductTypeList']),
        ];
    }

    public function getAttributeList(): array
    {
        return Attribute::getList();
    }

    public function getAttributeTemplate(string $attributeCode): string
    {
        switch ($attributeCode) {
            case Attribute::ATTRIBUTE_DIMENSIONS:
                $template = 'Product/attribute/dimensions.html.twig';

                break;

            default:
                $template = 'Product/attribute/default.html.twig';

                break;
        }

        return $template;
    }

    public function getProductList(): array
    {
        return Product::getList();
    }

    public function getProductTypeList(): array
    {
        return ProductService::getTypes();
    }
}
