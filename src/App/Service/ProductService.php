<?php

namespace App\Service;

use App\Model\Product;

class ProductService
{
    public static function getTypes(): array
    {
        return [
            Product::TYPE_DVD => 'DVD-disc',
            Product::TYPE_BOOK => 'Book',
            Product::TYPE_FURNITURE => 'Furniture',
        ];
    }
}
