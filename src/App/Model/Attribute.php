<?php

namespace App\Model;

use Core\AbstractModel;

/**
 * @Entity
 * @Table(name="attribute")
 */
class Attribute extends AbstractModel
{
    /**
     * @var string
     */
    public const ATTRIBUTE_SIZE = 'size';

    /**
     * @var string
     */
    public const ATTRIBUTE_WEIGHT = 'weight';

    /**
     * @var string
     */
    public const ATTRIBUTE_DIMENSIONS = 'dimensions';

    /**
     * @Id
     * @Column(type="integer")
     * @GeneratedValue
     */
    private $id;

    /**
     * @Column(type="string", name="attribute_code", unique=true)
     */
    private $attributeCode;

    /**
     * @Column(type="string", name="attribute_name")
     */
    private $attributeName;

    /**
     * @Column(type="string", name="allowed_types")
     */
    private $allowedTypes;

    /**
     * @Column(type="string")
     */
    private $note;

    /**
     * Get the value of id
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * Set the value of id
     */
    public function setId(int $id): self
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get the value of attributeCode
     */
    public function getAttributeCode(): string
    {
        return $this->attributeCode;
    }

    /**
     * Set the value of attributeCode
     */
    public function setAttributeCode(string $attributeCode): self
    {
        $this->attributeCode = $attributeCode;

        return $this;
    }

    /**
     * Get the value of attributeName
     */
    public function getAttributeName(): string
    {
        return $this->attributeName;
    }

    /**
     * Set the value of attributeName
     */
    public function setAttributeName(string $attributeName): self
    {
        $this->attributeName = $attributeName;

        return $this;
    }

    /**
     * Get the value of allowedTypes
     */
    public function getAllowedTypes(): array
    {
        return array_filter(explode(',', $this->allowedTypes));
    }

    /**
     * Set the value of allowedTypes
     */
    public function setAllowedTypes(array $allowedTypes): self
    {
        $this->allowedTypes = ',' . implode(',', $allowedTypes) . ',';

        return $this;
    }

    /**
     * Get the value of note
     */
    public function getNote(): string
    {
        return $this->note;
    }

    /**
     * Set the value of note
     */
    public function setNote(?string $note): self
    {
        $this->note = $note;

        return $this;
    }

    public static function getByAttributeCode(string $attributeCode): ?self
    {
        return self::getBy(['attributeCode' => $attributeCode]);
    }
}
