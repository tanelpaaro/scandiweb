<?php

namespace App\Model;

use Core\AbstractModel;

/**
 * @Entity
 * @Table(name="product_attribute_value", uniqueConstraints={@UniqueConstraint(name="unique_value", columns={"product_id", "attribute_id"})})
 */
class ProductAttributeValue extends AbstractModel
{
    /**
     * @Id
     * @Column(type="integer")
     * @GeneratedValue
     */
    private $id;

    /**
     * @Column(type="string")
     */
    private $value;

    /**
     * @ManyToOne(targetEntity="Product", inversedBy="product_attribute_value")
     * @JoinColumn(name="product_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $product;

    /**
     * @ManyToOne(targetEntity="Attribute")
     * @JoinColumn(name="attribute_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $attribute;

    /**
     * Get the value of id
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set the value of id
     */
    public function setId($id): self
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get the value of value
     */
    public function getValue(): string
    {
        return $this->value;
    }

    /**
     * Set the value of value
     *
     * @param mixed $value
     */
    public function setValue($value): self
    {
        $this->value = $this->formatValue($value);

        return $this;
    }

    /**
     * Get the value of product
     */
    public function getProduct(): Product
    {
        return $this->product;
    }

    /**
     * Set the value of product
     */
    public function setProduct(Product $product): self
    {
        $this->product = $product;

        return $this;
    }

    /**
     * Get the value of attribute
     */
    public function getAttribute(): Attribute
    {
        return $this->attribute;
    }

    /**
     * Set the value of attribute
     */
    public function setAttribute(Attribute $attribute): self
    {
        $this->attribute = $attribute;

        return $this;
    }

    /**
     * @param string|array $value
     */
    private function formatValue($value): string
    {
        $attributeCode = $this->getAttribute()->getAttributeCode();
        if (Attribute::ATTRIBUTE_DIMENSIONS === $attributeCode && is_array($value)) {
            $value = implode('x', $value);
        }

        return $value;
    }
}
