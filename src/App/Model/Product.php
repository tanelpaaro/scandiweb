<?php

namespace App\Model;

use Core\AbstractModel;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\PersistentCollection;

/**
 * @Entity
 * @Table(name="product")
 */
class Product extends AbstractModel
{
    public const TYPE_DVD = 'dvd';

    public const TYPE_BOOK = 'book';

    public const TYPE_FURNITURE = 'furniture';

    /**
     * @Id
     * @Column(type="integer")
     * @GeneratedValue
     */
    private $id;

    /**
     * @Column(type="string", unique=true)
     */
    private $sku;

    /**
     * @Column(type="string")
     */
    private $type;

    /**
     * @Column(type="string")
     */
    private $name;

    /**
     * @Column(type="decimal", precision=8, scale=2)
     */
    private $price;

    /**
     * @OneToMany(targetEntity="ProductAttributeValue", mappedBy="product", cascade={"persist"})
     */
    private $specialAttributes;

    public function __construct()
    {
        $this->specialAttributes = new ArrayCollection();
    }

    public function set($key, $value): bool
    {
        if (property_exists($this, $key)) {
            $method = 'set' . ucfirst($key);
            $this->{$method}($value);

            return true;
        }

        return false;
    }

    public function setSpecialAttributes(array $values): self
    {
        foreach (array_filter($values) as $field => $value) {
            $attribute = Attribute::getByAttributeCode($field);
            if ($attribute) {
                $productAttributeValue = new ProductAttributeValue();
                $productAttributeValue->setAttribute($attribute);
                $productAttributeValue->setValue($value);
                $productAttributeValue->setProduct($this);
                $this->addSpecialAttribute($productAttributeValue);
            }
        }

        return $this;
    }

    /**
     * Get the value of id
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * Set the value of id
     *
     * @return  self
     */
    public function setId($id): self
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get the value of sku
     */
    public function getSku(): string
    {
        return $this->sku;
    }

    /**
     * Set the value of sku
     *
     * @return  self
     */
    public function setSku($sku): self
    {
        $this->sku = $sku;

        return $this;
    }

    /**
     * Get the value of type
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * Set the value of type
     *
     * @return  self
     */
    public function setType($type): self
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get the value of name
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * Set the value of name
     *
     * @return  self
     */
    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get the value of price
     */
    public function getPrice(): float
    {
        return $this->price;
    }

    /**
     * Set the value of price
     *
     * @return  self
     */
    public function setPrice($price): self
    {
        $this->price = $price;

        return $this;
    }

    /**
     * @return ArrayCollection|PersistentCollection
     */
    public function getSpecialAttributes()
    {
        return $this->specialAttributes;
    }

    public function addSpecialAttribute(ProductAttributeValue $value): self
    {
        $this->specialAttributes->add($value);

        return $this;
    }
}
