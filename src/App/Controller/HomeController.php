<?php

namespace App\Controller;

use Core\AbstractController;

/**
 * Home controller
 */
class HomeController extends AbstractController
{
    public function indexAction(): void
    {
        header('Location: /product/list', true, 301);
    }
}
