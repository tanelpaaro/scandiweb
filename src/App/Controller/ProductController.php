<?php

namespace App\Controller;

use App\Model\Product;
use Core\AbstractController;
use Core\View;
use Doctrine\DBAL\Exception\UniqueConstraintViolationException;

class ProductController extends AbstractController
{
    public function addAction(): void
    {
        View::renderTemplate('Product/add.html.twig', ['data' => $this->getViewData()]);
    }

    public function addPostAction(): void
    {
        $postParams = $_POST;
        $product = new Product();
        foreach ($postParams as $field => &$value) {
            $value = $this->escapeValue($value);
            $_SESSION['product'][$field] = $value;
            if ($product->set($field, $value)) {
                unset($postParams[$field]);
            }
        }

        if ($postParams) {
            $product->setSpecialAttributes($postParams);
        }

        try {
            $product->save();
            $_SESSION['product']['successMessage'] = 'Product is saved.';
        } catch (UniqueConstraintViolationException $e) {
            $_SESSION['product']['errorMessage'] = 'Duplicate SKU';
        } catch (\Exception $e) {
            $_SESSION['product']['errorMessage'] = $e->getMessage();
        }

        header('Location: /product/add');
    }

    public function listAction(): void
    {
        View::renderTemplate('Product/list.html.twig', ['data' => $this->getViewData()]);
    }

    public function massDeleteAction(): void
    {
        $result = ['success' => false];

        $ids = $_POST['ids'] ?? [];
        foreach ($ids as $id) {
            $product = Product::getById($id);
            if ($product) {
                $product->delete();
            }
        }

        if ($ids) {
            $result = ['success' => true];
            $_SESSION['product']['successMessage'] = sprintf('Deleted %d product(s).', count($ids));
        }

        header('Content-Type: application/json; charset=utf-8');
        echo json_encode($result);
    }

    private function getViewData(): array
    {
        $data = $_SESSION['product'] ?? [];
        unset($_SESSION['product']);
        if (isset($data['successMessage'])) {
            $data = ['successMessage' => $data['successMessage']];
        }

        return $data;
    }
}
