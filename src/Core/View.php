<?php

namespace Core;

use App\Twig\AppExtension;
use Twig\Environment;
use Twig\Extension\DebugExtension;
use Twig\Extra\Intl\IntlExtension;
use Twig\Loader\FilesystemLoader;

/**
 * View
 */
class View
{
    /**
     * Render a view template using Twig
     *
     * @param string $template  The template file
     * @param array $args  Associative array of data to display in the view (optional)
     *
     * @return void
     */
    public static function renderTemplate($template, $args = [])
    {
        static $twig = null;

        if ($twig === null) {
            $loader = new FilesystemLoader(dirname(__DIR__) . '/../app/Resources/views');
            $twig = new Environment($loader); // ['debug' => true]

            $twig->addExtension(new AppExtension());
            $twig->addExtension(new IntlExtension());
            // $twig->addExtension(new DebugExtension());
        }

        echo $twig->render($template, $args);
    }
}
