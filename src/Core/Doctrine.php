<?php

namespace Core;

use App\Config;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Tools\Setup;

class Doctrine
{
    public static function getEntityManager(): EntityManager
    {
        static $entityManager = null;

        if (null === $entityManager) {
            $paths = [__DIR__ . '/../App/Model'];
            $isDevMode = false;
            $proxyDir = dirname(__DIR__) . '/../var/doctrine';

            $dbParams = [
                'driver'   => 'pdo_mysql',
                'user'     => Config::DB_USER,
                'password' => Config::DB_PASSWORD,
                'dbname'   => Config::DB_NAME,
                'host'     => Config::DB_HOST,
            ];

            $config = Setup::createAnnotationMetadataConfiguration($paths, $isDevMode, $proxyDir);
            $entityManager = EntityManager::create($dbParams, $config);
        }

        return $entityManager;
    }
}
