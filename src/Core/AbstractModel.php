<?php

namespace Core;

use Doctrine\ORM\EntityManager;

/**
 * Base model
 */
abstract class AbstractModel
{
    public function save(): self
    {
        $entityManager = $this->getEntityManager();
        $entityManager->persist($this);
        $entityManager->flush();

        return $this;
    }

    public static function getById(int $id): ?self
    {
        return self::getEntityManager()->find(static::class, $id);
    }

    public static function getBy(array $criteria): ?self
    {
        return self::getEntityManager()->getRepository(static::class)->findOneBy($criteria);
    }

    /**
     * @return static[]
     */
    public static function getList(): array
    {
        return self::getEntityManager()->getRepository(static::class)->findAll();
    }

    public function delete(): void
    {
        $entityManager = $this->getEntityManager();
        $entityManager->remove($this);
        $entityManager->flush();
    }

    protected static function getEntityManager(): EntityManager
    {
        return \Core\Doctrine::getEntityManager();
    }
}
