<?php

use Doctrine\ORM\Tools\Console\ConsoleRunner;

require __DIR__ . '/../vendor/autoload.php';

return ConsoleRunner::createHelperSet(\Core\Doctrine::getEntityManager());
