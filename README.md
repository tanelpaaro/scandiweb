# Overview #
Simple monolithic PHP application.  
Main keywords:  

- OOP - entities such as product, attribute and product attribute value.  
- EAV data model.  
- Presentation layer - MVC design; Twig.  
- Object persistence is handled with [Doctrine ORM](https://www.doctrine-project.org/projects/orm.html).  
- Data storage - MySQL database.  

# Installation #
1. Edit DB credentials - `src/App/Config.php`
2. `vendor/bin/doctrine orm:schema:create` - creates DB tables
3. `vendor/bin/doctrine orm:generate-proxies` - generates entity proxy classes
4. `bin/console app:setup:attributes` - creates attribute entities
5. `bin/console app:setup:products` - create product entities  

# Senior PHP & Web Developer test assignment  #

## General coding requirements

- Validation and business logic.
- Frameworks are restricted (no lumen, slim, Laravel, Symfony, etc.). Only composer
    
    packages are allowed.
    
- An Independent environment is an advantage.
- JS according to current best practices.
- Single Page Application is an advantage.
    
    
    ### These are the listed mandatory technical requirements:
    
    - Utilize **OOP principles** to handle differences in type logic/behavior
        - Procedural PHP code is allowed exclusively to initialize your PHP classes. Rest logic should be placed within class methods.
        - For OOP you would need to demonstrate code structuring in meaningful classes that extend each other, so we would like to see an abstract class for the main product logic. Please take a look at the polymorphism provision.
        - Also, MySQL logic should be handled by objects with properties instead of direct column values. Please use setters and getters for achieving this and don't forget to use them for both save and display logic.
    - Meet PSR standards ([https://www.php-fig.org](https://www.php-fig.org/))
    - Avoid using conditional statements for handling differences in product types
        - This means you should avoid any if-else and switch-case statements which are used to handle any difference between products.
    - Do not use different endpoints for different products types. There should be 1 general endpoint for product saving
    - PHP: ^7.0, plain classes, no frameworks, OOP approach
    - jQuery: optional
    - jQuery-UI: prohibited
    - Bootstrap: optional
    - SASS: advantage
    - MySQL: ^5.6 obligatory
    - 
    
    <aside>
    🚨 NOTE: React and vue.js is a huge advantage if you decide to use them for the frontend part, but not a requirement
    
    </aside>
    

**Task description:**

The expected outcome is 2 separate pages for:
     1)  product list
     2) product add

1. Product list should list all existing products and details, like: 

**a. SKU** (unique for each product)

**b. Name**

**c. Price**

Also, each product type has a special attribute, which we expect you would be able to display as well (one of based on type):
    a. Size (in MB) for *DVD-disc*
    b. Weight (in Kg) for *Book*
    c. Dimensions (HxWxL) for *Furniture*

*An* *advantage* would be the implementation of the optional feature: mass *delete* action, implemented as checkboxes next to each product.  

Here is an example of a product list page: 

![https://scandiweb.notion.site/image/https%3A%2F%2Fs3-us-west-2.amazonaws.com%2Fsecure.notion-static.com%2F8ef907af-38d3-4bc1-9547-37d3f5700d05%2FScreenshot_2021-07-29_at_14.36.16.png?table=block&id=eb754f7a-9b6c-486b-837d-d2bf3cf6166e&spaceId=196736dd-250f-45b1-ac1f-c4302855a2f9&width=2000&userId=&cache=v2](https://scandiweb.notion.site/image/https%3A%2F%2Fs3-us-west-2.amazonaws.com%2Fsecure.notion-static.com%2F8ef907af-38d3-4bc1-9547-37d3f5700d05%2FScreenshot_2021-07-29_at_14.36.16.png?table=block&id=eb754f7a-9b6c-486b-837d-d2bf3cf6166e&spaceId=196736dd-250f-45b1-ac1f-c4302855a2f9&width=2000&userId=&cache=v2)

2. Product add page should display a form, with the following fields 

    **a. SKU**

    **b. Name**

    **c. Price**

    **d. Type switcher (buttons for each type)**

    **e. Special attribute [please note: the form should be dynamically changed when type is switched]**

*Additional* *note:* Special attribute should have a description with helpful information, related to its type ex.: “Please provide dimensions in HxWxL format”, when type: Furniture is selected.

*Additional* ***requirement****:* all fields are mandatory for submission. *An* *advantage* would be fields value (or format whether suitable) validation.

Here is an example of a product add page:

![https://scandiweb.notion.site/image/https%3A%2F%2Fs3-us-west-2.amazonaws.com%2Fsecure.notion-static.com%2Fcfdab063-6086-4dea-accf-214302d85fb5%2FScreenshot_2021-07-29_at_14.37.45.png?table=block&id=879eee13-51a4-4756-9d99-403149b7e838&spaceId=196736dd-250f-45b1-ac1f-c4302855a2f9&width=2000&userId=&cache=v2](https://scandiweb.notion.site/image/https%3A%2F%2Fs3-us-west-2.amazonaws.com%2Fsecure.notion-static.com%2Fcfdab063-6086-4dea-accf-214302d85fb5%2FScreenshot_2021-07-29_at_14.37.45.png?table=block&id=879eee13-51a4-4756-9d99-403149b7e838&spaceId=196736dd-250f-45b1-ac1f-c4302855a2f9&width=2000&userId=&cache=v2)

## Finished? Now - time to AutoTest your work!

<aside>
💡 As already mentioned in the beginning of the task, AutoTesting with the result of "PASSED" is a **requirement** before submitting the task to HRs!

</aside>

This software helps you to understand what you might be missing in your assignment to us. It also helps you to check if all core elements of the assignment are working properly.

Just

1. Go to [AutoQA](http://159.89.47.246/)
2. Test your work!

If failed, please make the necessary adjustments to make it PASSED!

Once passed, please submit your test assignment to the HRs!

## How to submit?

1. Share the code as a Bitbucket repository that is shared with a user with [hr@scandiweb.com](mailto:hr@scandiweb.com) email.
2. Send the URL via email to your recruiter, where the above two pages “Product List” and “Add Product” will be available without a password and ready to be used, adding also the link to the Bitbucket repository.
    
    <aside>
    💡 Make sure the URL with this app is available 24/7 and is not dependent on your computer being “on” and connected. Only external deployment.
    
    </aside>
    
    <aside>
    💡 You can use your preferred or this free PHP and MySQL hosting: [https://www.000webhost.com/](https://www.000webhost.com/).
    
    </aside>
    

3. Add to the email a screenshot of PASSED AutoQA!