<?php

require __DIR__ . '/../vendor/autoload.php';

// Error and Exception handling
error_reporting(\E_ALL);
set_error_handler('Core\Error::errorHandler');
set_exception_handler('Core\Error::exceptionHandler');

$router = new Core\Router();
$router->add('', ['controller' => 'Home', 'action' => 'index']);
$router->add('product/list', ['controller' => 'Product', 'action' => 'list']);
$router->add('product/add', ['controller' => 'Product', 'action' => 'add']);
$router->add('product/addPost', ['controller' => 'Product', 'action' => 'addPost']);
$router->add('product/massDelete', ['controller' => 'Product', 'action' => 'massDelete']);
$router->dispatch($_SERVER['QUERY_STRING']);
