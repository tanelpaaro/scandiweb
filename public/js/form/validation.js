(function () {
    "use strict";

    var forms = document.querySelectorAll(".needs-validation");

    Array.prototype.slice.call(forms).forEach(function (form) {
        form.addEventListener("submit", function (event) {
            const isValid = form.checkValidity();
            if (!isValid) {
                event.preventDefault();
                event.stopPropagation();
            }

            form.classList.add("was-validated");
            if (isValid) {
                form.submit();
            }
        });
    });
})();
