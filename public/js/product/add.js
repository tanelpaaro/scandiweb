(function () {
    "use strict";

    document
        .getElementById("type")
        .addEventListener("change", function (event) {
            document
                .querySelectorAll(".special-attribute")
                .forEach(function (element) {
                    const allowedProductTypes = element.dataset.productTypes.split(
                            ","
                        ),
                        isHidden =
                            allowedProductTypes.indexOf(event.target.value) ===
                            -1;
                    element.classList.toggle("d-none", isHidden);
                    element.querySelectorAll("input").forEach(function (input) {
                        input.required = !isHidden;
                        input.disabled = isHidden;
                    });
                });
        });

    document.getElementById("type").dispatchEvent(new Event("change"));
})();

function submitForm(formId) {
    document.getElementById(formId).dispatchEvent(new Event("submit"));
}
