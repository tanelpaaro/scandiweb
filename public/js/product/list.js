(function () {
    "use strict";

    const productItemCheckboxSelector = ".product-item__checkbox:checked";

    document.querySelectorAll(".product-item").forEach(function (item) {
        const checkbox = item.querySelector("[type=checkbox]");

        checkbox.addEventListener("change", function (event) {
            let force = false;

            if (!this.checked) {
                force = !document.querySelectorAll(productItemCheckboxSelector)
                    .length;
            }

            document
                .getElementById("btnMassActionDelete")
                .toggleAttribute("disabled", force);
        });

        item.addEventListener("click", function (event) {
            if (event.target == checkbox) {
                return;
            }

            checkbox.checked = !checkbox.checked;
            checkbox.dispatchEvent(new Event("change"));
        });
    });

    document
        .getElementById("btnMassActionDelete")
        .addEventListener("click", function (event) {
            const items = document.querySelectorAll(
                    productItemCheckboxSelector
                ),
                formData = new FormData();

            if (items.length) {
                items.forEach(function (item) {
                    formData.append("ids[]", item.dataset.id);
                });

                window
                    .fetch("/product/massDelete", {
                        method: "POST",
                        headers: {
                            Accept: "application/json",
                        },
                        body: formData,
                    })
                    .then((response) => response.json())
                    .then((data) => {
                        if (data.success) {
                            location.reload();
                        }
                    });
            }
        });
})();
